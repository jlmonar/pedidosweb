@extends('layouts.app')

@section('title', 'Carrito de compras')

@section('content')
    <div class="container" style="margin-bottom: 22px">
        {{--
        <h1>Tu pedido</h1>
        --}}
        <hr>

        @if (!Cart::isEmpty())
            <div class="row">
                <div class="col-xs-12 col-sm-4 col-md-3">
                    <a href="{{ route('productos.index') }}" class="btn btn-primary btn-lg">Continuar comprando</a> &nbsp;
                </div>
                <div class="col-xs-12 col-sm-4 col-md-3">
                    {!! Form::open(['route' =>  'pedido.store', 'method' => 'POST']) !!}
                    {!! csrf_field() !!}
                    <input type="submit" class="btn btn-success btn-lg" value="Cerrar pedido">
                    {!!Form::close() !!}
                </div>
                <div class="col-xs-12 col-sm-4 col-md-6">
                    <form action="{{ route('clear-cart') }}" method="GET">
                        {!! csrf_field() !!}
                        <input type="hidden" name="_method" value="DELETE">
                        <input type="submit" class="btn btn-danger btn-lg pull-right" value="Vaciar carrito" onclick="return confirm('¿Seguro que desea vaciar carrito de pedido?')">
                    </form>
                </div>
            </div>

            <div class="panel panel-default" >
                <div class="panel-heading" style="font-weight: bold; font-size: 18px;">
                    Tu pedido
                    <a class="pull-right" href="{{route('pdf.index')}}" target="_blank" title="Descargar PDF" style="margin-right: 10px;">
                        <i class="fa fa-download" aria-hidden="true"></i>
                        {{--
                        <i class="fa fa-file-pdf-o" aria-hidden="true"></i>
                        --}}
                    </a>
                </div>
                <table class="table">
                    <thead>
                    <tr>
                        <th>Producto</th>
                        <th>Cantidad</th>
                        <th>Precio unitario</th>
                        <th>Total</th>
                        <th class="column-spacer"></th>
                        <th></th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach ($data as $item)
                        <tr>
                            <td width="40%"><a href="{{ route('productos.show', $item['id']) }}">{{ $item['name'] }}</a></td>
                            <td width="20%">
                                <div class="col-xs-12 col-sm-6 col-md-6" style="padding-left: 0px; padding-right: 0px">
                                    {!!Form::number('cantidad', $item['quantity'], ['class' => 'form-control', 'min' => '0','required', 'id' => 'qty-'.$item['id']]) !!}
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6" style="padding-left: 0px; padding-right: 0px">
                                    <button type="submit" class="btn btn-primary btn-edit" data-id="{{$item['id']}}">
                                        <i class="fa fa-refresh" aria-hidden="true"></i>
                                    </button>
                                </div>
                            </td>
                            <td width="20%">$ {{$item['price']}}</td>
                            <td width="15%">$ {{$item['price'] * $item['quantity']}}</td>
                            <td width="5%">
                                {!! Form::open(['route' =>  ['cart.destroy', $item['id']] , 'method' => 'DELETE']) !!}
                                {!! csrf_field() !!}
                                <input type="submit" class="btn btn-danger btn-sm" value="Eliminar" onclick="return confirm('¿Seguro que desea eliminarlo?')">
                                {!!Form::close() !!}
                            </td>
                        </tr>
                    @endforeach
                    <tr>
                        <td class="table-image"></td>
                        <td></td>
                        <td class="small-caps table-bg" style="text-align: right">Subtotal</td>
                        <td>$ {{ Cart::getSubtotal() }}</td>
                        <td></td>
                        <td></td>
                    </tr>

                    <tr class="border-bottom">
                        <td class="table-image"></td>
                        <td style="padding: 40px;"></td>
                        <td class="small-caps table-bg" style="text-align: right">Total a pagar</td>
                        <td class="table-bg">$ {{ Cart::getTotal() }}</td>
                        <td class="column-spacer"></td>
                        <td></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        @else
            <h3>No tienes items en tu carrito de compras</h3>
            <a href="{{ route('productos.index') }}" class="btn btn-primary btn-lg">Continuar comprando</a>
        @endif
    </div>
@endsection

@section('extra-js')
    <script>
        (function(){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $(".btn-edit").click(function(e) {
                var id = $(this).attr('data-id');
                var qty = $('#qty-' + id).val();

                $.ajax({
                    type: "PATCH",
                    url: '{{ url("/cart") }}' + '/' + id,
                    data: {
                        'quantity': qty,
                    },
                    success: function(data) {
                        window.location.href = '{{ url('/cart') }}';
                    }
                });
            });
        })();
    </script>
@endsection