@extends('layouts.app')

@section('title', 'Lista de productos')

@section('content')
    <div class="container">

        <div class="row">
            <div class="col-md-12" >
                <div id="panel-productos" class="panel panel-default">
                    <div class="panel-heading clearfix">
                        <h4 id="panel-title-productos" class="panel-title pull-left">Lista de productos</h4>
                        {{-- Buscador de productos --}}

                        {!! Form::model(Request::all(), ['route' => 'productos.index', 'method' => 'GET', 'class' => 'navbar-form pull-right']) !!}
                        <div class="form-group">
                            {!!Form::select('filtro', ['Nombre', 'Linea', 'Grupo', 'Sección'],null,['id'=>'filtro-tipo','class'=>'form-control'])!!}
                        </div>
                        <div class="form-group">
                            {!! Form::text('nombre', null, ['class' => 'form-control search-product', 'placeholder' => 'Nombre/Linea/Grupo/Sección...', 'aria-descridbedby' => 'search', 'required' => true]) !!}
                        </div>
                        <button type="submit" class="btn btn-default btn-search-product">Buscar</button>

                        {!!Form::close() !!}
                    </div>
                    <div class="panel-body">
                        <table id="table-productos" class="table table-bordered">
                            <thead>
                                <th>Nombre</th>
                                <th>Precio</th>
                                <th>Stock</th>
                                <th>Acción</th>
                                <th>
                                    <button id="agregar-masivo" class="btn btn-primary" href="#">
                                        {{-- Agregar --}}
                                        <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                    </button>
                                </th>
                            </thead>
                            @if(isset($productos))
                                <tbody>
                                @foreach($productos as $key=>$producto)
                                    @if(($key%2) == 0)
                                        <tr style="background-color: #e8e8e8;" id="tr-{{$producto->NUMERO_ITEM}}">
                                    @else
                                        <tr id="tr-{{$producto->NUMERO_ITEM}}">
                                    @endif
                                        <td>{{$producto->DESCRIPCION}}</td>
                                        @if ($nivel_precio_cliente == 1)
                                            <td>{{$producto->PRECIOD_VTA_1}}</td>
                                        @elseif ($nivel_precio_cliente == 2)
                                            <td>{{$producto->PRECIOD_VTA_2}}</td>
                                        @elseif ($nivel_precio_cliente == 3)
                                            <td>{{$producto->PRECIOD_VTA_3}}</td>
                                        @elseif ($nivel_precio_cliente == 4)
                                            <td>{{$producto->PRECIOD_VTA_4}}</td>
                                        @elseif ($nivel_precio_cliente == 5)
                                            <td>{{$producto->PRECIOD_VTA_5}}</td>
                                        @elseif ($nivel_precio_cliente == 6)
                                            <td>{{$producto->PRECIOD_VTA_6}}</td>
                                        @elseif ($nivel_precio_cliente == 7)
                                            <td>{{$producto->PRECIOD_VTA_7}}</td>
                                        @elseif ($nivel_precio_cliente == 8)
                                            <td>{{$producto->PRECIOD_VTA_8}}</td>
                                        @elseif ($nivel_precio_cliente == 9)
                                            <td>{{$producto->PRECIOD_VTA_9}}</td>
                                        @endif

                                        <td>{{$producto->STOCK_ACTUAL}}</td>
                                        <td style="text-align: center;">
                                            <a class="btn btn-warning" href="{{route('productos.show', $producto->NUMERO_ITEM)}}">Visualizar</a>
                                        </td>
                                        <td>
                                            {!! Form::number('cantidad', null, ['class' => 'form-control cantidad-producto', 'placeholder' => 'Cantidad', 'aria-descridbedby' => 'cantidad', 'style' => 'margin: auto;', 'min' => 1]) !!}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            @endif
                        </table>

                        @if(isset($productos))
                            <div class="text-center">
                                {{ $productos->appends(Request::all())->links() }}
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function() {
            $("#agregar-masivo").click(function() {
                /*
                * http://manohark.com/read-td-values-inside-select-input-tag-using-jquery/
                */
                var TableData = [];
                var i=0;

                $('#table-productos > tbody > tr').each(function(row, tr) {

                    var trId = this.id; // button ID
                    var arrayId = trId.split('-');
                    var cantidad = $('td:eq(4) input',this).val();

                    if (cantidad != '' && cantidad >0) {
                        TableData[i]={
                            "numeroItem": arrayId[1],
                            "cantidad": $('td:eq(4) input',this).val()
                        };
                        i++;
                    }
                });
                if ( i > 0) {
                    var outputValue=JSON.stringify(TableData);

                    window.location.replace("addItemsGroup/" + outputValue);
                } else {
                    alert('No ha ingresado la cantidad de ningún producto.');
                }
            });
        });
    </script>
@endsection