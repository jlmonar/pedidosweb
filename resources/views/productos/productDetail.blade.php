@extends('layouts.app')

@section('title', 'Detalle Producto')

@section('content')
    <div id='infoProducto' class="container" style="margin-bottom: 22px">
        <div class="row">
            <!-- Title -->
            <ol class="breadcrumb">
                <li><a href="{{ route('productos.index') }}">Lista productos</a></li>
                @if(isset($producto))
                    <li class="active">{{$producto->DESCRIPCION}}</li>
                @endif
            </ol>
        </div>
        <div class="row">
            <!-- Product Info-->
            @if(isset($producto))
                <div class="col-xs-12 col-sm-6">
                    <img src="../images/no_disponible.png" alt="Producto" class="img-responsive img_product">
                </div>
                <div class="col-xs-12 col-sm-6">
                    <h2 id="product-title">{{$producto->DESCRIPCION}} </h2>
                    <hr>

                    <h5>Disponible</h5>
                    <p class='product_description'>
                        Fully lined interior with zipper accessory pocket, metal feet on bottom to protect bag, accented with polished nickel hardware. Zippers top opening. 20.5 inch width x 12.5 inch height x 5.5 inch depth. ALG188
                    </p>

                    <br>
                    <p class="product_prize">$ {{number_format($producto->PRECIOD_VTA_1, 2)}}</p>

                    <br>
                    <label>Marca:</label>
                    <span>new marca</span>

                    <br><br>
                    <label>Código:</label>
                    <span>PRl0151</span>

                    <br><br>
                    <label>Stock:</label>
                    <span>{{$producto->STOCK_ACTUAL}}</span>

                    <br><br>

                    <div class="row">
                        {!! Form::open(['route' => ['add-item', $producto], 'method' => 'GET']) !!}
                        <div class="col-xs-5 col-sm-4">
                            {!!Form::number('cantidad', null, ['class' => 'form-control', 'min' => '0', 'placeholder' => 'Cantidad', 'required']) !!}
                        </div>
                        <div class="col-xs-3 col-sm-6">
                            <button type="submit" class="btn btn-info">
                                <i class="fa fa-cart-plus" aria-hidden="true"></i>
                            </button>
                        </div>
                        {!!Form::close() !!}
                    </div>
                </div>
            @endif
        </div>
    </div>
@endsection