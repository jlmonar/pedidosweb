<?php

namespace App\Http\Controllers\Producto;

use App\Models\ARBODB;
use App\Models\ARINDEX;
use App\Models\TBBIM_TIPOCL;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ProductoController extends Controller
{
    public function index(Request $request) {
        try {


            if (is_null($request->filtro) or $request->filtro == '0') {
                $productos = ARBODB::search($request->nombre)->orderBy('NUMERO_ITEM', 'DESC')->paginate(10);
            } else {
                $productos = null;
                switch ($request->filtro) {
                    case '1': {
                        $arindex = ARINDEX::where('NOMBRE', 'like', '%'.$request->nombre.'%')
                            ->where('DATA', 'L')->first();

                        if(!is_null($arindex)) {
                            $productos = ARBODB::where('LINEA', $arindex->CODIGO)->orderBy('NUMERO_ITEM', 'DESC')->paginate(10);
                        }

                        break;
                    }
                    case '2': {
                        $arindex = ARINDEX::where('NOMBRE', 'like', '%'.$request->nombre.'%')
                            ->where('DATA', 'G')->first();

                        if(!is_null($arindex)) {
                            $productos = ARBODB::where('GRUPO', $arindex->CODIGO)->orderBy('NUMERO_ITEM', 'DESC')->paginate(10);
                        }

                        break;
                    }
                    case '3': {
                        $arindex = ARINDEX::where('NOMBRE', 'like', '%'.$request->nombre.'%')
                            ->where('DATA', 'S')->first();

                        if(!is_null($arindex)) {
                            $productos = ARBODB::where('SECCION', $arindex->CODIGO)->orderBy('NUMERO_ITEM', 'DESC')->paginate(10);
                        }

                        break;
                    }
                }
            }

            $cliente = Auth::user();
            $tipoclte = $cliente->TIPOCLTE;
            if ($tipoclte == "0") {
                $tipoclte = 1;
            }

            $nivel_precio_cliente = TBBIM_TIPOCL::find($tipoclte)->TIP_NIVELPRE;

            return view('productos.index')->with([
                'productos' => $productos,
                'nivel_precio_cliente' => $nivel_precio_cliente
            ]);
        } catch (\Exception $ex) {
            return view('errors.503');
        }
    }

    public function show($idProducto) {
        $producto = ARBODB::find($idProducto);

        if ($producto == null) {
            return view('errors.405');
        }

        return view('productos.productDetail', compact('producto', $producto));
    }
}
