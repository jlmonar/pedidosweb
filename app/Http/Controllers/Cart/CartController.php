<?php

namespace App\Http\Controllers\Cart;

use App\Models\ARBODB;
//use Darryldecode\Cart\Cart;
use App\Models\TBBIM_TIPOCL;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Mockery\CountValidator\Exception;

use Cart;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cartCollection = Cart::getContent();
        $data = array_reverse($cartCollection->toArray());

        return view('cart.index')->with('data', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($request->quantity == 0) {
            Cart::remove($id);
            session()->flash('flash_message', '¡Producto eliminado del carrito!');
            session()->flash('alert_class', 'alert-success'); //Tambien hay la clase alert-warning
        } else {
            Cart::update($id, array(
                'quantity' => array(
                    'relative' => false,
                    'value' => $request->quantity
                ),
            ));
            session()->flash('flash_message', '¡Cantidad del producto actualizada!');
            session()->flash('alert_class', 'alert-success'); //Tambien hay la clase alert-warning
        }

        return response()->json(['success' => true]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Cart::remove($id);

        session()->flash('flash_message', '¡Producto eliminado del carrito!');
        session()->flash('alert_class', 'alert-success'); //Tambien hay la clase alert-warning

        return redirect()->route('cart.index');
    }

    public function addItem(Request $request, $id) {
        try {
            $producto = ARBODB::find($id);
            $precio = $this->getPrecioCliente($producto);

             Cart::add(array(
                'id' => $producto->NUMERO_ITEM,
                'name' => $producto->DESCRIPCION,
                'price' => $precio,
                'quantity' =>  $request->cantidad,
                'attributes' => array()
            ));
            session()->flash('flash_message', '¡Producto agregado al carrito!');
            session()->flash('alert_class', 'alert-success'); //Tambien hay la clase alert-warning
        } catch (Exception $ex) {
            return view('errors.405');
        } finally {
            return redirect()->route('cart.index');
        }
    }

    public function getPrecioCliente($producto) {
        $cliente = Auth::user();
        $tipoclte = $cliente->TIPOCLTE;
        if ($tipoclte == "0") {
            $tipoclte = "1";
        }

        $nivel_precio_cliente = TBBIM_TIPOCL::find($tipoclte)->TIP_NIVELPRE;
        switch ($nivel_precio_cliente) {
            case 1: {
                return $producto->PRECIOD_VTA_1;
            }
            case 2: {
                return $producto->PRECIOD_VTA_2;
            }
            case 3: {
                return $producto->PRECIOD_VTA_3;
            }
            case 4: {
                return $producto->PRECIOD_VTA_4;
            }
            case 5: {
                return $producto->PRECIOD_VTA_5;
            }
            case 6: {
                return $producto->PRECIOD_VTA_6;
            }
            case 7: {
                return $producto->PRECIOD_VTA_7;
            }
            case 8: {
                return $producto->PRECIOD_VTA_8;
            }
        }
    }

    public function addItemsGroup(Request $request, $items) {
        try {
            $array_items = json_decode($items);

            foreach ($array_items as $item) {
                $producto = ARBODB::find($item->numeroItem);
                $precio = $this->getPrecioCliente($producto);
                Cart::add(array(
                    'id' => $producto->NUMERO_ITEM,
                    'name' => $producto->DESCRIPCION,
                    'price' => $precio,
                    'quantity' =>  $item->cantidad,
                    'attributes' => array()
                ));
            }

            session()->flash('flash_message', '¡Productos agregados al carrito!');
            session()->flash('alert_class', 'alert-success'); //Tambien hay la clase alert-warning
        } catch (Exception $ex) {
            return view('errors.405');
        } finally {
            return redirect()->route('cart.index');
        }
    }

    public function clearCart() {
        Cart::clear();

        return redirect()->route('cart.index');
    }
}
