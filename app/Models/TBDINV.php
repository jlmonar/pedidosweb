<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TBDINV extends Model
{
    protected $table = 'TBDINV';

    protected $primaryKey = 'DINV_SEC';

    public $timestamps = false;

    public function producto()
    {
        return $this->belongsTo('App\Models\ARBODB', 'DINV_ITEM');
    }
}
