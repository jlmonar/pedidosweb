<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 30/07/2018
 * Time: 11:40
 */

namespace App\Models;


use App\Overrides\ModelCompositeKey;

class ARINDEX extends ModelCompositeKey
{
    protected $table = 'ARINDEX';

    protected $primaryKey =  array('EMPRESA', 'DATA', 'CODIGO');

    public $timestamps = false;
}