<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ARBODB extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'ARBODB';

    /**
     * Primary key for the table associated with the model.
     *
     * @var string
     */
    protected $primaryKey = 'NUMERO_ITEM';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    public function scopeSearch($query, $text) {
        return $query->where("DESCRIPCION", "LIKE", "%$text%");
    }
}
