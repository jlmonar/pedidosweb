<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//MIDDLEWARES
Route::middleware(['auth'])->group(function() {
    //Products routes
    Route::resource('productos', 'Producto\ProductoController');

    //Cart routes
    Route::resource('cart', 'Cart\CartController');
    Route::get('addItem/{id}', 'Cart\CartController@addItem')->name('add-item');
    Route::get('clearCart', 'Cart\CartController@clearCart')->name('clear-cart');
    Route::get('addItemsGroup/{items}', 'Cart\CartController@addItemsGroup')->name('add-items-group');

    Route::resource('pedido', 'TBCINV\TBCINVController');
    Route::get('historial_pedidos', 'TBCINV\TBCINVController@historialPedidos')->name('historial-pedidos');
    Route::get('consultar_pedidos', 'TBCINV\TBCINVController@consultarPedidos')->name('consultar-pedidos');

    Route::resource('detalle_pedido', 'TBDINV\TBDINVController');

    //Pdf
    Route::resource('pdf', 'PDF\PdfController');
});